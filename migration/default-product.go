package migration

import (
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"gitlab.com/jagertee/panama/model"
)

type defaultProducts struct{}

func (_ defaultProducts) Version() int {
	return 1
}

func (_ defaultProducts) Run(db *mgo.Database) error {
	p := model.Product{
		ID:          bson.NewObjectId(),
		Name:        "Akoya珍珠耳线",
		Thumbnail:   "https://panama.luois.ninja/images/pearl-1.jpg",
		Description: "Akoya珍珠耳线",
		Price:       998,
		OnSale:      true,
		Images: []string{
			"https://panama.luois.ninja/images/pearl-1.jpg",
			"https://panama.luois.ninja/images/pearl-2.jpg",
			"https://panama.luois.ninja/images/pearl-3.jpg",
			"https://panama.luois.ninja/images/pearl-4.jpg",
		},
	}
	err := model.CreateProduct(db, &p)
	return err
}
