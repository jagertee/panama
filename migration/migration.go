package migration

import (
	"log"
	"strconv"

	"github.com/globalsign/mgo"
	"gitlab.com/jagertee/panama/config"
)

type Migration interface {
	Version() int
	Run(*mgo.Database) error
}

var migrations = []Migration{
	defaultProducts{},
}

func newMigrations(version int) []Migration {
	var results []Migration
	for _, m := range migrations {
		if m.Version() > version {
			results = append(results, m)
		}
	}
	return results
}

func Run(db *mgo.Database) error {
	ver := 0
	dbver := config.Read(db, "migrationVersion")
	if dbver != "" {
		var err error
		ver, err = strconv.Atoi(dbver)
		if err != nil {
			return err
		}
	}
	migrations := newMigrations(ver)
	for _, migration := range migrations {
		log.Printf("running db migration version: %d", migration.Version())
		err := migration.Run(db)
		if err != nil {
			return err
		}
		newVer := strconv.Itoa(migration.Version())
		err = config.Save(db, "migrationVersion", newVer)
		if err != nil {
			return err
		}
	}
	return nil
}
