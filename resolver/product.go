package resolver

import (
	"gitlab.com/jagertee/panama/model"
)

type ProductResolver struct {
	*model.Product
}

func (r *ProductResolver) ID() ObjectId {
	return ObjectId{r.Product.ID}
}

func (r *ProductResolver) Name() *string {
	return &r.Product.Name
}

func (r *ProductResolver) Thumbnail() *string {
	return &r.Product.Thumbnail
}

func (r *ProductResolver) Description() *string {
	return &r.Product.Description
}

func (r *ProductResolver) Price() float64 {
	return r.Product.Price
}

func (r *ProductResolver) Images() *[]string {
	return &r.Product.Images
}

func (r *ProductResolver) OnSale() bool {
	return r.Product.OnSale
}
