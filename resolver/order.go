package resolver

import (
	graphql "github.com/graph-gophers/graphql-go"
	"gitlab.com/jagertee/panama/model"
)

type OrderResolver struct {
	*model.Order
	product *model.Product
}

func (r *OrderResolver) ID() ObjectId {
	return ObjectId{r.Order.ID}
}

func (r *OrderResolver) Price() float64 {
	return r.Order.Price
}

func (r *OrderResolver) Status() string {
	return r.Order.Status.String()
}

func (r *OrderResolver) Address() *OrderAddressResolver {
	return &OrderAddressResolver{&r.Order.Address}
}

func (r *OrderResolver) CreatedAt() graphql.Time {
	return graphql.Time{r.Order.CreatedAt}
}

func (r *OrderResolver) Product() *ProductResolver {
	return &ProductResolver{r.product}
}

type OrderAddressResolver struct {
	*model.OrderAddress
}

func (r *OrderAddressResolver) UserName() string {
	return r.OrderAddress.UserName
}

func (r *OrderAddressResolver) PostalCode() string {
	return r.OrderAddress.PostalCode
}

func (r *OrderAddressResolver) ProvinceName() string {
	return r.OrderAddress.ProvinceName
}

func (r *OrderAddressResolver) CityName() string {
	return r.OrderAddress.CityName
}

func (r *OrderAddressResolver) CountyName() string {
	return r.OrderAddress.CountyName
}

func (r *OrderAddressResolver) DetailInfo() string {
	return r.OrderAddress.DetailInfo
}

func (r *OrderAddressResolver) NationalCode() string {
	return r.OrderAddress.NationalCode
}

func (r *OrderAddressResolver) TelNumber() string {
	return r.OrderAddress.TelNumber
}
