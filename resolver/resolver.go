package resolver

import (
	"context"
	"fmt"
	// "log"
	// graphql "github.com/graph-gophers/graphql-go"

	"github.com/globalsign/mgo"
	"gitlab.com/jagertee/panama/model"
)

// commom input type
type IDInput struct {
	ID ObjectId
}

type Resolver struct {
	db *mgo.Database
}

func NewResolver(db *mgo.Database) *Resolver {
	return &Resolver{db}
}

func getUser(ctx context.Context) (*model.User, error) {
	if user, ok := ctx.Value("User").(*model.User); ok {
		return user, nil
	}
	return nil, fmt.Errorf("Forbiddon")
}

func (r *Resolver) OnSaleProduct() (*ProductResolver, error) {
	p, err := model.GetOnSaleProdcut(r.db)
	if err != nil {
		return nil, err
	}

	return &ProductResolver{p}, nil
}

func (r *Resolver) Product(ctx context.Context, args IDInput) (*ProductResolver, error) {
	p, err := model.GetProduct(r.db, args.ID.ObjectId)
	if err != nil {
		return nil, err
	}

	return &ProductResolver{p}, nil
}

func (r *Resolver) Orders(ctx context.Context) (*[]*OrderResolver, error) {
	user, err := getUser(ctx)
	if err != nil {
		return nil, err
	}

	orders, err := model.GetUserOrders(r.db, user.ID)
	if err != nil {
		return nil, err
	}

	resp := []*OrderResolver{}
	for _, order := range *orders {
		p, err := model.GetProduct(r.db, order.ProductID)
		if err != nil {
			return nil, err
		}
		resp = append(resp, &OrderResolver{&order, p})
	}
	return &resp, nil
}

func (r *Resolver) Order(ctx context.Context, args IDInput) (*OrderResolver, error) {
	user, err := getUser(ctx)
	if err != nil {
		return nil, err
	}

	order, err := model.GetOrder(r.db, user.ID, args.ID.ObjectId)
	if err != nil {
		return nil, err
	}
	p, err := model.GetProduct(r.db, order.ProductID)
	if err != nil {
		return nil, err
	}
	return &OrderResolver{order, p}, nil
}

func (r *Resolver) Hell() string {
	return ""
}
