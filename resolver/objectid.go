package resolver

import (
	"fmt"

	"github.com/globalsign/mgo/bson"
)

type ObjectId struct {
	bson.ObjectId
}

func (_ ObjectId) ImplementsGraphQLType(name string) bool {
	return name == "ObjectId"
}

func (o *ObjectId) UnmarshalGraphQL(input interface{}) error {
	switch input := input.(type) {
	case bson.ObjectId:
		o.ObjectId = input
		return nil
	case string:
		if bson.IsObjectIdHex(input) {
			o.ObjectId = bson.ObjectIdHex(input)
			return nil
		}
	}
	return fmt.Errorf("%v is not a valid ObjectId", input)
}
