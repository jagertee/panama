FROM scratch

ENV GIN_MODE release

ADD .env .
ADD main .
COPY static ./static

EXPOSE 3001

CMD ["/main"]
