package config

import (
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

const CONFIG_COL = "configs"

type Config struct {
	Key   string `bson:"key"`
	Value string `bson:"value"`
}

func Read(db *mgo.Database, key string) string {
	c := Config{}
	err := db.C(CONFIG_COL).Find(bson.M{"key": key}).One(&c)
	if err != nil {
		return ""
	}
	return c.Value
}

func Save(db *mgo.Database, key string, value string) error {
	c := Config{
		Key:   key,
		Value: value,
	}

	_, err := db.C(CONFIG_COL).Upsert(bson.M{"key": key}, &c)
	return err
}
