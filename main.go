package main

import (
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"github.com/globalsign/mgo"
	"github.com/joho/godotenv"
	"gitlab.com/jagertee/panama/handler"
	"gitlab.com/jagertee/panama/migration"
	"gitlab.com/jagertee/panama/resolver"

	graphql "github.com/graph-gophers/graphql-go"
)

func loadSchema() (string, error) {
	bs, err := ioutil.ReadFile("schema.graphql")
	if err != nil {
		return "", err
	}
	return string(bs), nil
}

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Println("Cannot load envs: ", err)
	}

	session, err := mgo.Dial(os.Getenv("DBADDR"))
	if err != nil {
		log.Fatalf("error connecting to db: %v", err)
	}
	defer session.Close()
	db := session.DB("panama")

	// read schema
	schemaString, err := loadSchema()
	if err != nil {
		log.Fatalf("error loading schema: %v", err)
	}
	resolver := resolver.NewResolver(db)

	schema := graphql.MustParseSchema(schemaString, resolver)
	graphqlHandler := handler.NewGraphqlHandler(schema)

	// migrations
	err = migration.Run(db)
	if err != nil {
		log.Fatalf("cannot run db migrations: %v", err)
	}

	mux := http.NewServeMux()
	mux.HandleFunc("/hc", healthCheck)
	mux.HandleFunc("/login", handler.NewLoginEndpoint(db))
	mux.HandleFunc("/graphiql", handler.GraphiQLEndpoint)
	mux.Handle("/graphql", graphqlHandler)

	// TODO graceful shutdown
	http.ListenAndServe(":3001", mux)
}

func healthCheck(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("It's working."))
}
