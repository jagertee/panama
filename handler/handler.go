package handler

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/globalsign/mgo"
	graphql "github.com/graph-gophers/graphql-go"
	"github.com/graph-gophers/graphql-go/relay"
	"gitlab.com/jagertee/panama/model"
)

type M map[string]interface{}

func NewGraphqlHandler(schema *graphql.Schema) http.Handler {
	return &relay.Handler{schema}
}

func responseJson(w http.ResponseWriter, code int, body interface{}) {
	w.WriteHeader(code)
	r, _ := json.Marshal(body)
	w.Write(r)
}

func GraphiQLEndpoint(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "static/graphiql.html")
}

func NewLoginEndpoint(db *mgo.Database) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodPost {
			w.WriteHeader(http.StatusMethodNotAllowed)
			return
		}
		params := struct{ Code string }{}
		err := json.NewDecoder(r.Body).Decode(&params)
		if err != nil {
			responseJson(w, http.StatusBadRequest, M{
				"error": "must provide code",
			})
			return
		}
		log.Printf("recieve code: %v", params.Code)

		token, err := model.Login(db, params.Code)
		if err != nil {
			log.Printf("login failed: %v", err)
			responseJson(w, http.StatusUnauthorized, M{
				"error": "invalid code",
			})
			return
		}

		responseJson(w, http.StatusOK, M{
			"token": token,
		})
	}
}
