package wechat

import (
	"time"
)

type MemCache struct {
	data map[string]interface{}
}

func (c *MemCache) Get(key string) interface{} {
	return c.data[key]
}

func (c *MemCache) Set(key string, val interface{}, _ time.Duration) error {
	c.data[key] = val
	return nil
}

func (c *MemCache) IsExist(key string) bool {
	_, exist := c.data[key]
	return exist
}

func (c *MemCache) Delete(key string) error {
	delete(c.data, key)
	return nil
}
