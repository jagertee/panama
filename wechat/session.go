package wechat

import (
	"encoding/json"
	"errors"
	"log"
	"os"

	request "github.com/parnurzeal/gorequest"
	"github.com/silenceper/wechat"
)

type WechatSession struct {
	OpenID     string `json:"openid"`
	SessionKey string `json:"session_key"`
	UnionID    string `json:"unionid"`
}

var config = wechat.Config{}

func GetSessionKey(code string) (WechatSession, error) {
	session := WechatSession{}

	appid := os.Getenv("WECHAT_APPID")
	appsecret := os.Getenv("WECHAT_APPSECRET")
	if appid == "" || appsecret == "" {
		return session, errors.New("wechat appid or secret not set")
	}

	req := request.New().Get("https://api.weixin.qq.com/sns/jscode2session")
	req.Param("appid", appid)
	req.Param("secret", appsecret)
	req.Param("js_code", code)
	req.Param("grant_type", "authorization_code")

	_, body, errs := req.EndBytes()
	if errs != nil {
		log.Println(errs)
		return session, nil
	}
	err := json.Unmarshal([]byte(body), &session)
	if err != nil {
		return session, err
	}

	return session, nil
}

func CreateOrder() {

}
