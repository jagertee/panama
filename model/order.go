package model

import (
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

const ORDER_COL = "orders"

type OrderStatus int

const (
	OrderStatusUnpaid OrderStatus = iota
	OrderStatusPaid
	OrderStatusSent
	OrderStatusFinished
	OrderStatusCancel
)

func (s OrderStatus) String() string {
	names := []string{
		"待付款",
		"待发货",
		"已发货",
		"已签收",
		"已取消",
	}
	return names[s]
}

type OrderAddress struct {
	UserName     string `bson:"userName" json:"userName"`
	PostalCode   string `bson:"postalCode" json:"postalCode"`
	ProvinceName string `bson:"provinceName" json:"provinceName"`
	CityName     string `bson:"cityName" json:"cityName"`
	CountyName   string `bson:"countyName" json:"countyName"`
	DetailInfo   string `bson:"detailInfo" json:"detailInfo"`
	NationalCode string `bson:"nationalCode" json:"nationalCode"`
	TelNumber    string `bson:"telNumber" json:"telNumber"`
}

type Order struct {
	ID         bson.ObjectId `bson:"_id" json:"id"`
	UserID     bson.ObjectId `bson:"userId" json:"_"`
	ProductID  bson.ObjectId `bson:"productId" json:"-"`
	ExtraJSON  string        `bson:"extraJson" json:"-"`
	ResultJSON string        `bson:"resultJson" json:"-"`
	Price      float64       `bson:"price" json:"price"`
	Status     OrderStatus   `bson:"status" json:"status"`
	Address    OrderAddress  `bson:"address" json:"address"`
	CreatedAt  time.Time     `bson:"createdAt" json:"createdAt"`
	UpdatedAt  time.Time     `bson:"updatedAt" json:"updatedAt"`
}

func CreateOrder(db *mgo.Database, userID bson.ObjectId, productID bson.ObjectId, address OrderAddress) (*Order, error) {
	p, err := GetProduct(db, productID)
	if err != nil {
		return nil, err
	}

	// TODO generate wechat order
	order := Order{
		ID:        bson.NewObjectId(),
		UserID:    userID,
		ProductID: productID,
		Price:     p.Price,
		Address:   address,
		Status:    OrderStatusUnpaid,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}

	err = db.C(ORDER_COL).Insert(&order)
	if err != nil {
		return &order, err
	}

	return &order, nil
}

func GetUserOrders(db *mgo.Database, userID bson.ObjectId) (*[]Order, error) {
	var orders []Order
	err := db.C(ORDER_COL).Find(bson.M{"userId": userID}).Sort("-createdAt").All(&orders)

	return &orders, err
}

func GetOrder(db *mgo.Database, userID bson.ObjectId, orderID bson.ObjectId) (*Order, error) {
	var order Order
	err := db.C(ORDER_COL).Find(bson.M{
		"_id":    orderID,
		"userId": userID,
	}).One(&order)

	return &order, err
}

func CancelOrder(db *mgo.Database, userID bson.ObjectId, orderID bson.ObjectId) error {
	return db.C(ORDER_COL).Update(bson.M{
		"_id":    orderID,
		"userId": userID,
	}, bson.M{
		"$set": bson.M{
			"status":    OrderStatusCancel,
			"updatedAt": time.Now(),
		},
	})
}

func PayOrder(db *mgo.Database, userID bson.ObjectId, orderID bson.ObjectId) error {
	return db.C(ORDER_COL).Update(bson.M{
		"_id":    orderID,
		"userId": userID,
	}, bson.M{
		"$set": bson.M{
			"status":    OrderStatusPaid,
			"updatedAt": time.Now(),
		},
	})
}
