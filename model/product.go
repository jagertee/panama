package model

import (
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

const PRODUCT_COL = "products"

// import "log"
type Product struct {
	ID          bson.ObjectId `bson:"_id"`
	Name        string        `bson:"name"`
	Thumbnail   string        `bson:"thumbnail"`
	Description string        `bson:"description"`
	Price       float64       `bson:"price"`
	Images      []string      `bson:"images"`
	OnSale      bool          `bson:"onSale"`
}

func CreateProduct(db *mgo.Database, product *Product) error {
	return db.C(PRODUCT_COL).Insert(&product)
}

func GetProduct(db *mgo.Database, productID bson.ObjectId) (*Product, error) {
	product := Product{}
	err := db.C(PRODUCT_COL).FindId(productID).One(&product)
	return &product, err
}

func GetOnSaleProdcut(db *mgo.Database) (*Product, error) {
	product := Product{}
	err := db.C(PRODUCT_COL).Find(bson.M{"onSale": true}).One(&product)
	return &product, err
}
