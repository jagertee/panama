package model

import (
	"log"
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/google/uuid"
	"gitlab.com/jagertee/panama/wechat"
)

const USER_COL = "users"
const SESSION_COL = "sessions"

type User struct {
	ID               bson.ObjectId `bson:"_id,omitempty"`
	Name             string        `bson:"name"`
	Avatar           string        `bson:"avatar"`
	WechatId         string        `bson:"wechatId"`
	WechatOpenId     string        `bson:"wechatOpenId"`
	WechatSessionKey string        `bson:"wechatSessionKey"`
	CreatedAt        time.Time     `bson:"createdAt"`
	UpdatedAt        time.Time     `bson:"updatedAt"`
}

type WechatInfo struct {
	UnionID string
	OpenID  string
	Name    string
	Avatar  string
}

type Session struct {
	UserID bson.ObjectId `bson:"userId"`
	Token  string        `bson:"token"`
}

func Authorize(db *mgo.Database, token string) (*User, error) {
	sc := db.C(SESSION_COL)
	session := Session{}

	err := sc.Find(bson.M{"token": token}).One(&session)
	if err != nil {
		return nil, err
	}

	uc := db.C(USER_COL)
	user := User{}

	err = uc.FindId(session.UserID).One(&user)

	return &user, err
}

func Login(db *mgo.Database, code string) (string, error) {
	info, err := wechat.GetSessionKey(code)
	if err != nil {
		return "", err
	}

	var user User
	err = db.C(USER_COL).Find(bson.M{"wechatOpenId": info.OpenID}).One(&user)

	if err == mgo.ErrNotFound {
		user = User{
			ID:               bson.NewObjectId(),
			WechatId:         info.UnionID,
			WechatOpenId:     info.OpenID,
			WechatSessionKey: info.SessionKey,
			CreatedAt:        time.Now(),
			UpdatedAt:        time.Now(),
		}
		err = db.C(USER_COL).Insert(&user)
		if err != nil {
			return "", err
		}
	} else if err != nil {
		return "", err
	}

	// find Session
	session := Session{
		UserID: user.ID,
		Token:  uuid.New().String(),
	}
	_, err = db.C(SESSION_COL).Upsert(bson.M{
		"userId": user.ID,
	}, &session)

	if err != nil {
		return "", err
	}
	return session.Token, nil
}

func fetchWechatInfo(code string) (*WechatInfo, error) {
	s, err := wechat.GetSessionKey(code)
	if err != nil {
		return nil, err
	}
	log.Printf("OpenId: %v", s.OpenID)

	return &WechatInfo{
		Name:    "A.I.",
		OpenID:  "wechatopenid",
		UnionID: "wechatid",
		Avatar:  "https://google.com",
	}, nil
}
